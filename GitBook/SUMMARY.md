# Summary

* [Business Understanding](business_understanding/intro.md)
* [Data Understanding](data_understanding/intro.md)
* [Data Preparation](data_preparation/intro.md)
* [Modelling](modelling/intro.md)
* [Evaluation and Deployment](evaluation_and_deployment/intro.md)
   * [Evaluation](evaluation_and_deployment/evaluation.md)
   * [Deployment](evaluation_and_deployment/deployment.md)

