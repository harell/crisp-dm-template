This project introduces you to several notions:

1. Working in a standard process, specifically the [CRISP-DM](https://en.wikipedia.org/wiki/Cross_Industry_Standard_Process_for_Data_Mining "Read in Wikipedia").
2. Authoring dynamic documents and reports from R which are fully reproducible.
3. Publish books using Git and Markdown.


