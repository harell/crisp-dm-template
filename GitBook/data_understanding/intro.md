# Data Understanding

The data understanding phase starts with an initial data collection and proceeds with activities in order to get familiar with the data, to identify data quality problems, to discover first insights into the data, or to detect interesting subsets to form hypotheses for hidden information.

* Data Objects and Attribute Types
* Basic Statistical Descriptions of Data
* Data Visualization
* Measuring Data Similarity and Dissimilarity
* Summary
* Exercises
* Bibliographic Notes
