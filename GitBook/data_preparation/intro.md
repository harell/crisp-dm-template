# Data Preparation

The data preparation phase covers all activities to construct the final dataset (data that will be fed into the modeling tool(s)) from the initial raw data. Data preparation tasks are likely to be performed multiple times, and not in any prescribed order. Tasks include table, record, and attribute selection as well as transformation and cleaning of data for modeling tools.

* Data Preprocessing: An Overview
* Data Cleaning
  * Outlier Detection
  * Dealing with Missing Values
* Data Integration
* Data Reduction
* Data Transformation and Data Discretization
* Summary
#  Data Preparation
