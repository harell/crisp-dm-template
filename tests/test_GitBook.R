source(file.path("lib","GitBook.r"))
## Create the GitBook from the SUMMMARY
generateGitBook(dir="GitBook")
## Convert .Rmd files to .md files
knitGitBook(dir="GitBook")