## This function takes a dir, search for SUMMARY.md file, 
## read it's contant and repredouces folders and files according to that information.
## NOTE: The function defualt is not to overwrite files.
generateGitBook <- function(dir=getwd(), overwrite=FALSE){
        require(knitr)
        con <- file.path(dir,paste0("SUMMARY",".md")) 
        SUMMARY <- readLines(con,skipNul=TRUE)
        
        ## 1. Make dirs and files
        ## 1.1. extract the string enclosed in parenthesis
        filepath <- sub(".*\\((.*)\\).*", "\\1", SUMMARY, perl=TRUE)
        ## 1.2. replace .md with .Rmd extension
        filepath <- sub(".md", ".Rmd", filepath, perl=FALSE)
        ## 1.3. BUG fix: remove first row & remove empty strings
        filepath <- filepath[-1]
        filepath <- filepath[!filepath %in% ""]
        ## 1.4. Check existence of directory and create if doesn't exist
        dirpath <- sub("(.*)/(.*).Rmd", "\\1", filepath, perl=TRUE)
        sapply(file.path(dir, dirpath),dir.create,showWarnings=FALSE,recursive=TRUE)
        #invisible()
        ## 1.5. Create file if it doesn't exist      
        Is.File.Exists <- file.exists(file.path(dir,filepath))
        for (i in 1:length(Is.File.Exists)){
                if(!Is.File.Exists[i] | overwrite){
                        file.create(file.path(dir,filepath[i]), showWarnings = FALSE, overwrite=overwrite)
                }
        }
        
        ## 2. Make headline in each file
        ## 2.1. extract the string enclosed in brackets
        headline <- sub(".*\\[(.*)\\].*", "\\1", SUMMARY, perl=TRUE) 
        ## 2.2. BUG fix: remove first row & remove empty strings
        headline <- headline[-1]
        headline <- headline[!headline %in% ""]        
        for (i in 1:length(headline)){
                if (!Is.File.Exists[i]){
                        fileConn <- file(file.path(dir,filepath[i]))
                        writeLines(paste("#",headline[i]), fileConn)
                        close(fileConn)                        
                }
        }        
}

## This function search for knit Rmd files to md files
knitGitBook <- function(dir=getwd()){
        require(knitr)
        con <- file.path(dir,paste0("SUMMARY",".md")) 
        SUMMARY <- readLines(con,skipNul=TRUE)
        ## 1. Get files list       
        ## 1.1. extract the string enclosed in parenthesis
        filepath <- sub(".*\\((.*)\\).*", "\\1", SUMMARY, perl=TRUE)
        ## 1.2. remove file name/file suffix
        dirpath  <- sub("(.*)/(.*).md", "\\1", filepath, perl=TRUE)
        filepath <- sub(".md", "", filepath, perl=FALSE)
        ## 1.3. BUG fix: remove first row & remove empty strings
        filepath <- filepath[-1]
        filepath <- filepath[!filepath %in% ""]
        dirpath  <- dirpath[-1]
        dirpath  <- dirpath[!dirpath %in% ""]        
        
        ## 2. knit documents 
        for (i in 1:length(dirpath)){
                opts_knit$set(base.dir = file.path(dir,dirpath[i]))
                input  <- file.path(dir,paste0(filepath[i],".Rmd"))     
                output <- file.path(dir,paste0(filepath[i],".md"))
                #knit2html(input, output, quiet=T) 
                invisible(knit(input, output, quiet=T)) 
        }
}
