Cross-Industry Standard Process for Data Mining
========================================================

To load your the project, you'll first need to `setwd()` into the directory
where this README file is located. Then you need to run the following two
lines of R code:

```
library("ProjectTemplate")
load.project()
```

## If you know what you're doing, run the following lines (where `GitBook` is the book's folder name)

```
## Create the GitBook from the SUMMMARY
generateGitBook(dir="GitBook")
## Convert .Rmd files to .md files
knitGitBook(dir="GitBook")
```

## Else, follow the instructions

To create GitBook you need to make a folder for the book, and include three file within it:

1. `book.json` should be an empty file
2. `README.md` should contain verbal project introduction
3. `SUMMARY.md` should contain the table of contents of the project, for example
  
```
# Summary

* [Business Understanding](business_understanding/intro.md)
* [Data Understanding](data_understanding/intro.md)
* [Data Preparation](data_preparation/intro.md)
* [Modelling](modelling/intro.md)
* [Evaluation and Deployment](evaluation_and_deployment/intro.md)
   * [Evaluation](evaluation_and_deployment/evaluation.md)
   * [Deployment](evaluation_and_deployment/deployment.md)
```

You also need to source the following file:

* `GitBook.r` (currently at _lib_ dir) and don't forget to source it.

The project contains a template (CRISP-DM framework) for your disposal